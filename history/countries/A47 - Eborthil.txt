government = monarchy
add_government_reform = plutocratic_reform
government_rank = 2
primary_culture = tefori
religion = regent_court
technology_group = tech_cannorian
national_focus = DIP
capital = 380

1000.1.1 = { set_country_flag = mage_organization_magisterium_flag }

1402.6.1 = {
	monarch = {
		name = "Delian II"
		dynasty = "Silebor"
		birth_date = 1398.6.20
		adm = 4
		dip = 4
		mil = 4
	}
}

1422.1.1 = { set_country_flag = lilac_wars_moon_party }

1423.11.2 = {
	heir = {
		name = "Arman"
		monarch_name = "Arman IV"
		dynasty = "Silebor"
		birth_date = 1423.11.2
		death_date = 1499.4.4
		claim = 95
		adm = 5
		dip = 5
		mil = 3
	}
}
