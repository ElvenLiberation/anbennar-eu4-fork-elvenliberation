government = monarchy
add_government_reform = feudalism_reform
government_rank = 1
primary_culture = arannese
religion = regent_court
technology_group = tech_cannorian
national_focus = DIP
capital = 311

1000.1.1 = { set_country_flag = mage_organization_magisterium_flag }

1422.1.1 = { set_country_flag = lilac_wars_moon_party }

1422.1.2 = { set_country_flag = is_a_county }

1441.2.3 = {
	monarch = {
		name = "Valeran III"
		dynasty = "s�l Vivin"
		birth_date = 1427.9.5
		adm = 2
		dip = 2
		mil = 4
	}
}