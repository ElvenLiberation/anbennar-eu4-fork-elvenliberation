# No previous file for H10
government = theocracy
add_government_reform = monastic_order_reform
government_rank = 1
primary_culture = tuathak
religion = eordellon
technology_group = tech_eordand
capital = 2159

1421.7.5 = {
	monarch = {
		name = "Galinaldor"
		dynasty = "Oceansong"
		adm = 5
		dip = 4
		mil = 2
		birth_date = 1418.6.10
	}
	add_ruler_personality = mage_personality
	set_ruler_flag = divination_1
	set_ruler_flag = transmutation_1
	set_ruler_flag = transmutation_2
	add_ruler_personality = industrious_personality
}