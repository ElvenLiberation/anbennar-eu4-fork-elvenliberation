#581 - Yamet�es

owner = F18
controller = F18
add_core = F18
culture = bahari
religion = bulwari_sun_cult

base_tax = 1
base_production = 2
base_manpower = 1

trade_goods = fish

capital = ""

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari