#267 - Plsen

owner = A95
controller = A95
add_core = A95
culture = esmari
religion = regent_court

hre = yes

base_tax = 11
base_production = 9
base_manpower = 5

trade_goods = cloth

capital = ""

is_city = yes
fort_15th = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish
