# No previous file for Mantua
owner = A45
controller = A45
add_core = A45
culture = east_damerian
religion = regent_court

hre = yes

base_tax = 5
base_production = 5
base_manpower = 4

trade_goods = cloth

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish