# No previous file for Lakota
owner = H04
controller = H04
add_core = H04
culture = selphereg
religion = eordellon
capital = "Elcos"
fort_15th = yes

hre = no

base_tax = 5
base_production = 4
base_manpower = 3

trade_goods = incense

native_size = 14
native_ferocity = 6
native_hostileness = 6