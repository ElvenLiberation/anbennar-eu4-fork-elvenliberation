#47 - Stralsund | 

owner = Z05
controller = Z05
add_core = Z05
culture = pearlsedger
religion = regent_court

hre = yes

base_tax = 3
base_production = 3
base_manpower = 2

trade_goods = fish
capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish