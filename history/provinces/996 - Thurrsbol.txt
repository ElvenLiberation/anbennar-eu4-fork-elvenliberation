# No previous file for Thurrsbol
owner = Z17
controller = Z17
add_core = Z17
culture = fjord_troll
religion = animism

hre = no

base_tax = 3
base_production = 3
base_manpower = 2

trade_goods = fish

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_gnomish
discovered_by = tech_giantkind