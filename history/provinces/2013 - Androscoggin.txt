# No previous file for Androscoggin
owner = H01
controller = H01
add_core = H01
culture = tuathak
religion = eordellon
capital = "Androscoggin"

hre = no

base_tax = 2
base_production = 1
base_manpower = 1

trade_goods = wool

native_size = 14
native_ferocity = 6
native_hostileness = 6