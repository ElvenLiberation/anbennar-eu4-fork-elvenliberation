#269 - Podlasia

owner = A35
controller = A35
add_core = A35
culture = esmari
religion = regent_court

hre = yes

base_tax = 5
base_production = 4
base_manpower = 3

trade_goods = livestock

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish
