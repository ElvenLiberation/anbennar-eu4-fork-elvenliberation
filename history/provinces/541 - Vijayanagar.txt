#541 - Eduz-szel-Ninezim

owner = F15
controller = F15
add_core = F15
add_core = F19
culture = sun_elf
religion = bulwari_sun_cult

base_tax = 3
base_production = 3
base_manpower = 3

trade_goods = incense

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari

add_nationalism = 10
