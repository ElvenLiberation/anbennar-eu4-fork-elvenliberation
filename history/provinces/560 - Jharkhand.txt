# No previous file for Jharkhand
owner = F40
controller = F40
add_core = F40
culture = brasanni
religion = bulwari_sun_cult

hre = no

base_tax = 5
base_production = 7
base_manpower = 4

trade_goods = slaves
center_of_trade = 1

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari