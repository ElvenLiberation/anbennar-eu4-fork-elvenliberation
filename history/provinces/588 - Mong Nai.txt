# No previous file for Mong Nai
owner = F37
controller = F37
add_core = F37
culture = brasanni
religion = bulwari_sun_cult

hre = no

base_tax = 5
base_production = 4
base_manpower = 2

trade_goods = cotton
center_of_trade = 2

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari