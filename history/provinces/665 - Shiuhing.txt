# No previous file for Shiuhing
owner = F26
controller = F26
add_core = F26
culture = gelkar
religion = bulwari_sun_cult


base_tax = 7
base_production = 7
base_manpower = 4

trade_goods = cloth
center_of_trade = 2

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari