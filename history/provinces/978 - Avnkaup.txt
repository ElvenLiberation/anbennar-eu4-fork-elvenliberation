# No previous file for Avnkaup
owner = Z14
controller = Z14
add_core = Z14
culture = olavish
religion = skaldhyrric_faith

hre = no

base_tax = 2
base_production = 2
base_manpower = 2

trade_goods = naval_supplies

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_gnomish
discovered_by = tech_giantkind