#345 - Abda | 

owner = A24
controller = A24
add_core = A24
culture = moorman
religion = regent_court

hre = no

base_tax = 4
base_production = 4
base_manpower = 4

trade_goods = wool

capital = ""

is_city = yes
fort_15th = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold


