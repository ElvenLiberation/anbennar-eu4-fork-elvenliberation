#83 - Nassau | 

owner = A01
controller = A01
add_core = A01
culture = low_lorentish
religion = regent_court

hre = no

base_tax = 4
base_production = 3
base_manpower = 1

trade_goods = grain

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
