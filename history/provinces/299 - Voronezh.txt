#299 - Voronezh

owner = A30
controller = A30
culture = east_damerian
religion = regent_court

hre = yes

base_tax = 4
base_production = 5
base_manpower = 2

trade_goods = iron

capital = ""

is_city = yes
fort_15th = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish

