#115 - Pisa | Ioriellen

owner = A03
controller = A03
add_core = A03

culture = moon_elf
religion = regent_court

hre = no

base_tax = 6
base_production = 6
base_manpower = 3

trade_goods = silk
center_of_trade = 1

capital = "Ioriel's Glade"

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish

extra_cost = 10
