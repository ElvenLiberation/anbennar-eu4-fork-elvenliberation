# No previous file for Navanagar
culture = caamas
religion = eordellon
capital = "Navanagar"

hre = no

base_tax = 2
base_production = 1
base_manpower = 2

trade_goods = unknown

native_size = 14
native_ferocity = 6
native_hostileness = 6