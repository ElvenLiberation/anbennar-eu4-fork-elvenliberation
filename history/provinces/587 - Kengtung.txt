# No previous file for Kengtung
owner = Z03
controller = Z03
add_core = Z03
culture = east_damerian
religion = regent_court

hre = yes

base_tax = 6
base_production = 6
base_manpower = 4

trade_goods = paper

capital = ""

is_city = yes
fort_15th = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish