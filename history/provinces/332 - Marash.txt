#332 - Marash

owner = A34
controller = A34
add_core = A34
culture = wexonard
religion = regent_court

hre = yes

base_tax = 5
base_production = 4
base_manpower = 3

trade_goods = cloth
capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
