find_reincarnated_warrior_aspect {
	sprite = "GFX_dissolve_monasteries_icon"
	cost = 50
	
	effect = {
		create_general = { tradition = 60 }
	}
	
	ai_will_do = {
		factor = 8
		modifier = {
			factor = 0
			num_of_generals = 2
		}
	}
}

gather_great_host_aspect {
	sprite = "GFX_dissolve_monasteries_icon"
	cost = 200
	
	effect = {
		country_event = { id = great_dookan_events.3 }
	}
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			OR = {
				AND = {
					manpower_percentage = 0.1
					army_size_percentage = 0.1
				}
				NOT = { is_at_war = yes }
			}
		}
	}
}

glorious_looting_aspect {
	sprite = "GFX_dissolve_monasteries_icon"
	cost = 150
	
	trigger = {
		is_at_war = yes
		any_province = {
			controlled_by = ROOT
			NOT = { owned_by = ROOT }
		}
	}
	
	effect = {
		country_event = { id = great_dookan_events.4 }
	}
	
	ai_will_do = {
		factor = 5
	}
}

purge_the_weak_aspect {
	sprite = "GFX_dissolve_monasteries_icon"
	cost = 100
	
	trigger = {
		is_at_war = no
		any_owned_province = {
			OR = {
				has_owner_religion = no
				has_owner_religion = no
			}
		}
	}
	
	effect = {
		country_event = { id = great_dookan_events.2 }
	}
	
	ai_will_do = {
		factor = 3
		modifier = {
			factor = 0
			is_at_war = yes
		}
	}
}

invoke_warrior_heritage_aspect {
	sprite = "GFX_dissolve_monasteries_icon"
	cost = 100

	effect = {
		country_event = { id = great_dookan_events.1 }
	}
	
		
	ai_will_do = {
		factor = 1000
		modifier = {
			factor = 0
			NOT = {
				any_province = {
					unit_in_battle = yes
					num_of_units_in_province = {
						amount = 6
					}
					num_of_units_in_province = {
						who = ROOT
						amount = 3
					}
				}
			}
		}
	}
}
