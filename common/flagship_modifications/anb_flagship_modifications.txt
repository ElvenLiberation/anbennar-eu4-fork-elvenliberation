nimscodd_the_heirarch = {
	trigger = {
		has_country_modifier = nimscodd_the_heirarch_designed
	}
	modifier = {
		flagship_durability = 2
		number_of_cannons_flagship_modifier = 2
		naval_maintenance_flagship_modifier = 3
	}
	ai_trade_score = {
		factor = 0
    }
	ai_war_score = {
		factor = 10
	}
}