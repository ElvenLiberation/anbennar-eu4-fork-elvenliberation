monstrous_culture = {
	OR = {
		culture_group = kobold
		culture_group = orcish
		culture_group = gnollish
		culture_group = goblinoid
		culture_group = harpy
		culture_group = giantkind
	}
}

has_no_ward_province_modifiers = {
	NOT = { has_province_modifier = magic_realm_abjuration_empowered_ward}
	NOT = { has_province_modifier = magic_realm_abjuration_ward}
	NOT = { has_province_modifier = magic_estate_ward }
}

culture_group_is_short_lived = {
	OR = {
		culture_group = anbennarian
		culture_group = alenic
		culture_group = businori
		culture_group = escanni
		culture_group = dostanian
		culture_group = divenori
		culture_group = lencori
		culture_group = halfling
		culture_group = gerudian
		culture_group = gnollish
		culture_group = orcish
		culture_group = goblinoid
		culture_group = bulwari
	}
}

culture_group_is_human = {
	OR = {
		culture_group = anbennarian
		culture_group = alenic
		culture_group = businori
		culture_group = escanni
		culture_group = dostanian
		culture_group = divenori
		culture_group = lencori
		culture_group = bulwari
		#whatever new aelantiri is
	}
}

enhance_ability_cost_adm_trigger = {
	if = {
		limit = { adm = 0 }
		adm_power = 100
	}
	else_if = {
		limit = { adm = 1 }
		adm_power = 100
	}
	else_if = {
		limit = { adm = 2 }
		adm_power = 200
	}
	else_if = {
		limit = { adm = 3 }
		adm_power = 300
	}
	else_if = {
		limit = { adm = 4 }
		adm_power = 400
	}
	else_if = {
		limit = { adm = 5 }
		adm_power = 500
	}
}

enhance_ability_cost_dip_trigger = {
	if = {
		limit = { dip = 0 }
		dip_power = 100
	}
	else_if = {
		limit = { dip = 1 }
		dip_power = 100
	}
	else_if = {
		limit = { dip = 2 }
		dip_power = 200
	}
	else_if = {
		limit = { dip = 3 }
		dip_power = 300
	}
	else_if = {
		limit = { dip = 4 }
		dip_power = 400
	}
	else_if = {
		limit = { dip = 5 }
		dip_power = 500
	}
}

enhance_ability_cost_mil_trigger = {
	if = {
		limit = { mil = 0 }
		mil_power = 100
	}
	else_if = {
		limit = { mil = 1 }
		mil_power = 100
	}
	else_if = {
		limit = { mil = 2 }
		mil_power = 200
	}
	else_if = {
		limit = { mil = 3 }
		mil_power = 300
	}
	else_if = {
		limit = { mil = 4 }
		mil_power = 400
	}
	else_if = {
		limit = { mil = 5 }
		mil_power = 500
	}
}



province_with_farm_goods = {
	OR = {
		trade_goods = grain
		trade_goods = livestock
		trade_goods = wine
		trade_goods = spices
		trade_goods = tea
		trade_goods = coffee
		trade_goods = cocoa
		trade_goods = cotton
		trade_goods = sugar
		trade_goods = tobacco
		trade_goods = silk
	}
}

is_abjuration_1 = {
		has_ruler_flag = abjuration_1
		NOT = { has_ruler_flag = abjuration_2 }
		NOT = { has_ruler_flag = abjuration_3 }
}

is_abjuration_2 = {
		has_ruler_flag = abjuration_2
		NOT = { has_ruler_flag = abjuration_3 }
}

is_abjuration_3 = {
		has_ruler_flag = abjuration_3
}

is_conjuration_1 = {
		has_ruler_flag = conjuration_1
		NOT = { has_ruler_flag = conjuration_2 }
		NOT = { has_ruler_flag = conjuration_3 }
}

is_conjuration_2 = {
		has_ruler_flag = conjuration_2
		NOT = { has_ruler_flag = conjuration_3 }
}

is_conjuration_3 = {
		has_ruler_flag = conjuration_3
}

is_divination_1 = {
		has_ruler_flag = divination_1
		NOT = { has_ruler_flag = divination_2 }
		NOT = { has_ruler_flag = divination_3 }
}

is_divination_2 = {
		has_ruler_flag = divination_2
		NOT = { has_ruler_flag = divination_3 }
}

is_divination_3 = {
		has_ruler_flag = divination_3
}

is_enchantment_1 = {
		has_ruler_flag = enchantment_1
		NOT = { has_ruler_flag = enchantment_2 }
		NOT = { has_ruler_flag = enchantment_3 }
}

is_enchantment_2 = {
		has_ruler_flag = enchantment_2
		NOT = { has_ruler_flag = enchantment_3 }
}

is_enchantment_3 = {
		has_ruler_flag = enchantment_3
}

is_evocation_1 = {
		has_ruler_flag = evocation_1
		NOT = { has_ruler_flag = evocation_2 }
		NOT = { has_ruler_flag = evocation_3 }
}

is_evocation_2 = {
		has_ruler_flag = evocation_2
		NOT = { has_ruler_flag = evocation_3 }
}

is_evocation_3 = {
		has_ruler_flag = evocation_3
}

is_illusion_1 = {
		has_ruler_flag = illusion_1
		NOT = { has_ruler_flag = illusion_2 }
		NOT = { has_ruler_flag = illusion_3 }
}

is_illusion_2 = {
		has_ruler_flag = illusion_2
		NOT = { has_ruler_flag = illusion_3 }
}

is_illusion_3 = {
		has_ruler_flag = illusion_3
}

is_necromancy_1 = {
		has_ruler_flag = necromancy_1
		NOT = { has_ruler_flag = necromancy_2 }
		NOT = { has_ruler_flag = necromancy_3 }
}

is_necromancy_2 = {
		has_ruler_flag = necromancy_2
		NOT = { has_ruler_flag = necromancy_3 }
}

is_necromancy_3 = {
		has_ruler_flag = necromancy_3
}

is_transmutation_1 = {
		has_ruler_flag = transmutation_1
		NOT = { has_ruler_flag = transmutation_2 }
		NOT = { has_ruler_flag = transmutation_3 }
}

is_transmutation_2 = {
		has_ruler_flag = transmutation_2
		NOT = { has_ruler_flag = transmutation_3 }
}

is_transmutation_3 = {
		has_ruler_flag = transmutation_3
}

precursor_relics_can_spawn = {
	is_city = yes
	NOT = {
		trade_goods = precursor_relics
		trade_goods = damestear
		trade_goods = gold
		has_global_flag = max_precursor_relics
	}
	OR = {
		AND = {
			OR = {
				is_year = 1560
				has_global_flag = castellos_is_dead
			}
			REB = {
				NOT = {
					check_variable = {
						which = num_precursor_relics
						value = 5
					}
				}
			}
		}
		
		AND = {
			is_year = 1580
			REB = {
				NOT = {
					check_variable = {
						which = num_precursor_relics
						value = 10
					}
				}
			}
		}
		
		AND = {
			is_year = 1600
			REB = {
				NOT = {
					check_variable = {
						which = num_precursor_relics
						value = 15
					}
				}
			}
		}
		
		AND = {
			is_year = 1620
			REB = {
				NOT = {
					check_variable = {
						which = num_precursor_relics
						value = 20
					}
				}
			}
		}
		
		AND = {
			is_year = 1650
			REB = {
				NOT = {
					check_variable = {
						which = num_precursor_relics
						value = 30
					}
				}
			}
		}
	}
}

damestear_can_spawn = {
	any_owned_province = {
		is_city = yes
		NOT = {
			trade_goods = precursor_relics
			trade_goods = damestear
			trade_goods = gold
			has_province_flag = has_damestear
		}
	}
	OR = {
		AND = {
			is_year = 1450
			REB = {
				NOT = {
					check_variable = {
						which = num_damestear
						value = 3
					}
				}
			}
		}
		
		AND = {
			is_year = 1520
			REB = {
				NOT = {
					check_variable = {
						which = num_damestear
						value = 12
					}
				}
			}
		}
		
		AND = {
			is_year = 1590
			REB = {
				NOT = {
					check_variable = {
						which = num_damestear
						value = 15
					}
				}
			}
		}
		AND = {
			is_year = 1650
			REB = {
				NOT = {
					check_variable = {
						which = num_damestear
						value = 20
					}
				}
			}
		}
	}
}

relics_great_ruin = {
	NOT = {
		REB = {
			check_variable = {
				which = num_great_relic
				value = 5
			}
		}
	}
}
