# Regimented Bigscales

type = cavalry
unit_type = tech_kobold

maneuver = 2
offensive_morale = 5
defensive_morale = 3
offensive_fire = 1
defensive_fire = 0
offensive_shock = 4
defensive_shock = 2
