gnomish_weapon_research_act = {
	monarch_power = MIL

	potential = {
		OR = {
			tag = A06
			tag = A79
		}
	}
	allow = {
		has_country_modifier = nimscodd_new_prototypes_developed
	}
	
	innovativeness_gain = 0.5
	mil_tech_cost_modifer = -0.1
	fire_damage = 0.2

	ai_will_do = {
		factor = 10		
	}
}