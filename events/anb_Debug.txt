namespace = anb_debug

#Give Me Witch Kings
country_event = {
	id = anb_debug.1
	title = anb_debug.1.t
	desc = anb_debug.1.d
	picture = HUIZTILOPOCHTLI_eventPicture
	
	is_triggered_only = yes
	
	# Come forth!
	option = {
		name = anb_debug.1.a
		ai_chance = {
			factor = 90
		}
		
		increase_witch_king_points_large = yes
		increase_witch_king_points_large = yes
		increase_witch_king_points_large = yes
		increase_witch_king_points_large = yes
		increase_witch_king_points_large = yes
		increase_witch_king_points_large = yes
		increase_witch_king_points_large = yes
		increase_witch_king_points_large = yes
		increase_witch_king_points_large = yes
		increase_witch_king_points_large = yes
		
	}
}