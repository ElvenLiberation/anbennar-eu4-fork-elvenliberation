
namespace = flavor_istralore

#Corinites in the Imperial Army
country_event = {
	id = flavor_istralore.1
	title = flavor_istralore.1.t
	desc = flavor_istralore.1.d
	picture = LAND_MILITARY_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = A45
		
		current_age = age_of_reformation
		
		NOT = { religion = corinite }
	}
	
	mean_time_to_happen = {
		months = 100
		
		modifier = {
			factor = 0.5
			any_owned_province = { 
				religion = corinite
			}
		}
		
		modifier = {
			factor = 0.1
			any_owned_province = { 
				religion = corinite
				is_reformation_center = yes
			}
		}
	}
	
	#Convert the entire country
	option = {
		name = flavor_istralore.1.a
		ai_chance = {
			factor = 60
			modifier = {
				factor = 10000
				has_personal_deity = corin
			}
			modifier = {
				factor = 2
				ruler_has_personality = zealot_personality
			}
			modifier = {
				factor = 2
				mil = 3
			}
			modifier = {
				factor = 0
				is_emperor = yes
			}
			modifier = {
				factor = 2
				personality = ai_militarist
			}
			modifier = {
				factor = 1.5
				ruler_has_personality = free_thinker_personality
			}
		}
		change_religion = corinite
		
		capital_scope = {
			change_religion = corinite
			add_reform_center = corinite
			add_permanent_province_modifier = {
				name = "religious_zeal_at_conv"
				duration = 9000
			}
		}
		
		add_ruler_modifier = { 
			name = istralore_support_of_the_corinite_military
			duration = -1  
		}
	}
	
	#Corinite Crusaders revolt
	option = {
		name = flavor_istralore.1.b
		ai_chance = {
			factor = 40
			modifier = {
				factor = 10000
				has_personal_deity = adean
			}
			modifier = {
				factor = 1.5
				ruler_has_personality = calm_personality
			}
			modifier = {
				factor = 1.5
				ruler_has_personality = careful_personality
			}
		}
		
		random_owned_province = {
			limit = {
				religion_group = cannorian
				is_core = ROOT 
				development = 10
				is_capital = no
			}
			change_religion = corinite
			spawn_rebels = {
				type = corinite_rebels
				size = 3
				win = yes
			}
		}
		
	}

}