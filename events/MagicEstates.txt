namespace = magic_estate

#Magic Menu
country_event = {
	id = magic_estate.1
	title = magic_estate.1.t
	desc = magic_estate.1.d
	picture = BIG_BOOK_eventPicture
	
	is_triggered_only = yes
	
	
	option = {	
		name = magic_estate.1.a	#go back
		ai_chance = {
			factor = 5
		}
		#some sort of tooltip
	}
	
	# option = {	#Illegally transmute gold removed for now
		# name = magic_estate.2.t
		# ai_chance = {
			# factor = 50
		# }
		# country_event = { id = magic_estate.2 days = 0 }
	# }
	
	option = {	#Host Magical Feast
		name = magic_estate.3.t
		ai_chance = {
			factor = 50
			modifier = {
				factor = 2
				personality = ai_diplomat
			}
		}
		country_event = { id = magic_estate.3 days = 0 }
	}
	
	option = {	#Encourage Plant Growth
		name = magic_estate.4.t
		ai_chance = {
			factor = 50
			modifier = {
				factor = 2
				personality = ai_capitalist
			}
		}
		country_event = { id = magic_estate.4 days = 0 }
	}
	
	option = {	#Scrying
		name = magic_estate.5.t
		ai_chance = {
			factor = 50
			modifier = {
				factor = 2
				is_at_war = yes
			}
		}
		country_event = { id = magic_estate.5 days = 0 }
	}
	
	option = {	#Aid Constructions
		name = magic_estate.6.t
		ai_chance = {
			factor = 50
			modifier = {
				factor = 2
				any_owned_province = {
					construction_progress = 0.1
				}
			}
		}
		country_event = { id = magic_estate.6 days = 0 }
	}
	
	option = {	#Ward
		name = magic_estate.7.t
		ai_chance = {
			factor = 50
			modifier = {
				factor = 2
				is_at_war = yes
			}
			modifier = {
				factor = 2
				personality = ai_capitalist
			}
		}
		country_event = { id = magic_estate.7 days = 0 }
	}
	
	option = {	#Rite of Conception
		name = magic_estate.8.t
		ai_chance = {
			factor = 50
			modifier = {
				factor = 10
				has_consort = yes
				has_heir = no
				ruler_has_personality = mage_personality 
			}
		}
		country_event = { id = magic_estate.8 days = 0 }
	}
}

#Illegally transmute gold - UNUSED
# country_event = {
	# id = magic_estate.2
	# title = magic_estate.2.t
	# desc = magic_estate.2.d
	# picture = CORRUPTION_eventPicture
	
	# is_triggered_only = yes
	
	# option = {	#Transmute a humble amount into fake gold
		# name = magic_estate.2.a
		# ai_chance = {
			# factor = 50
			# modifier = {
				# factor = 0.8
				# NOT = { years_of_income = 0.125 }
			# }
		# }
		# add_years_of_income = 2
		# add_inflation = 15
		# random_list =  {
			# 25 = {
				# add_country_modifier = {
					# name = magic_estate_caught_illegally_transmuting_gold
					# duration = 3650
				# }
			# }
			# 75 = {
			
			# }
		# }
		# add_estate_influence_modifier = {
			# estate = estate_mages
			# desc = EST_VAL_CASTED_SPELLS
			# influence = -10
			# duration = 3650
		# }
	# }
	
	# option = {	#I want everything you touch to turn into gold!
		# name = magic_estate.2.b
		# ai_chance = {
			# factor = 50
		# }
		# add_years_of_income = 5
		# add_inflation = 30
		# add_corruption = 1
		# random_list =  {
			# 50 = {
				# add_country_modifier = {
					# name = magic_estate_caught_illegally_transmuting_gold
					# duration = 3650
				# }
			# }
			# 50 = {
			
			# }
		# }
		# add_estate_influence_modifier = {
			# estate = estate_mages
			# desc = EST_VAL_CASTED_SPELLS
			# influence = -10
			# duration = 3650
		# }
	# }
	
	# option = {	#Option B: Go back
		# name = magic_estate.goback
		# ai_chance = {
			# factor = 5
		# }
		# country_event = { id = magic_estate.1 days = 0 }
	# }
	
# }

#Host Magical Feast
country_event = {
	id = magic_estate.3
	title = magic_estate.3.t
	desc = magic_estate.3.d
	picture = FEAST_eventPicture
	
	is_triggered_only = yes
	
	option = {	#A year's worth of events should do
		name = magic_estate.3.a
		ai_chance = {
			factor = 50
		}
		
		#Cost
		add_years_of_income = -0.5
		
		#Effect
		add_country_modifier = {
			name = magic_estate_hosting_magical_feast
			duration = 365
		}
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_CASTED_SPELLS
			influence = -10
			duration = 3650
		}
	}
	
	option = {	#Option B: Go back
		name = magic_estate.goback
		ai_chance = {
			factor = 5
		}
		country_event = { id = magic_estate.1 days = 0 }
	}
	
}


#Encourage Plant Growth
country_event = {
	id = magic_estate.4
	title = magic_estate.4.t
	desc = magic_estate.4.d
	picture = FARMING_eventPicture
	
	is_triggered_only = yes
	
	immediate = {
		hidden_effect = { 
			count_provinces_with_farm_goods_effect = {
				which = amount_of_farm_good_provinces
			}
		}
	}
	
	option = {	#Option A: give modifier boost to all provinces
		name = magic_estate.4.a
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0
				check_variable = { which = amount_of_farm_good_provinces value = 100 }
			}
		}
	
		if = {
			limit = {
				check_variable = { which = amount_of_farm_good_provinces value = 1 }
				NOT = { check_variable = { which = amount_of_farm_good_provinces value = 5 } }
			}
			add_years_of_income = -0.25
		}
		if = {
			limit = {
				check_variable = { which = amount_of_farm_good_provinces value = 5 }
				NOT = { check_variable = { which = amount_of_farm_good_provinces value = 10 } }
			}
			add_years_of_income = -0.5
		}
		if = {
			limit = {
				check_variable = { which = amount_of_farm_good_provinces value = 10 }
				NOT = { check_variable = { which = amount_of_farm_good_provinces value = 15 } }
			}
			add_years_of_income = -0.75
		}
		if = {
			limit = {
				check_variable = { which = amount_of_farm_good_provinces value = 15 }
				NOT = { check_variable = { which = amount_of_farm_good_provinces value = 20 } }
			}
			add_years_of_income = -1
		}
		if = {
			limit = {
				check_variable = { which = amount_of_farm_good_provinces value = 20 }
				NOT = { check_variable = { which = amount_of_farm_good_provinces value = 30 } }
			}
			add_years_of_income = -1.5
		}
		if = {
			limit = {
				check_variable = { which = amount_of_farm_good_provinces value = 30 }
				NOT = { check_variable = { which = amount_of_farm_good_provinces value = 40 } }
			}
			add_years_of_income = -2
		}
		if = {
			limit = {
				check_variable = { which = amount_of_farm_good_provinces value = 40 }
				NOT = { check_variable = { which = amount_of_farm_good_provinces value = 50 } }
			}
			add_years_of_income = -2.5
		}
		if = {
			limit = {
				check_variable = { which = amount_of_farm_good_provinces value = 50 }
				NOT = { check_variable = { which = amount_of_farm_good_provinces value = 60 } }
			}
			add_years_of_income = -3
		}
		if = {
			limit = {
				check_variable = { which = amount_of_farm_good_provinces value = 60 }
				NOT = { check_variable = { which = amount_of_farm_good_provinces value = 70 } }
			}
			add_years_of_income = -3.5
		}
		if = {
			limit = {
				check_variable = { which = amount_of_farm_good_provinces value = 70 }
				NOT = { check_variable = { which = amount_of_farm_good_provinces value = 80 } }
			}
			add_years_of_income = -4
		}
		if = {
			limit = {
				check_variable = { which = amount_of_farm_good_provinces value = 80 }
				NOT = { check_variable = { which = amount_of_farm_good_provinces value = 90 } }
			}
			add_years_of_income = -4.5
		}
		if = {
			limit = {
				check_variable = { which = amount_of_farm_good_provinces value = 90 }
				NOT = { check_variable = { which = amount_of_farm_good_provinces value = 100 } }
			}
			add_years_of_income = -5
		}
		if = {
			limit = {
				check_variable = { which = amount_of_farm_good_provinces value = 100 }
				NOT = { check_variable = { which = amount_of_farm_good_provinces value = 200 } }
			}
			add_years_of_income = -7.5
		}
		if = {
			limit = {
				check_variable = { which = amount_of_farm_good_provinces value = 200 }
				NOT = { check_variable = { which = amount_of_farm_good_provinces value = 300 } }
			}
			add_years_of_income = -10
		}
		if = {
			limit = {
				check_variable = { which = amount_of_farm_good_provinces value = 300 }
				NOT = { check_variable = { which = amount_of_farm_good_provinces value = 400 } }
			}
			add_years_of_income = -12
		}
		if = {
			limit = {
				check_variable = { which = amount_of_farm_good_provinces value = 400 }
				NOT = { check_variable = { which = amount_of_farm_good_provinces value = 500 } }
			}
			add_years_of_income = -13
		}
		if = {
			limit = {
				check_variable = { which = amount_of_farm_good_provinces value = 500 }
				NOT = { check_variable = { which = amount_of_farm_good_provinces value = 1000 } }
			}
			add_years_of_income = -14
		}
		
		every_owned_province = { 
			limit = { province_with_farm_goods = yes }
			add_province_modifier = { 
				name = magic_estate_plant_growth
				
				duration = 3650
			}
		}
		
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_CASTED_SPELLS
			influence = -10
			duration = 3650
		}
	}
		
	option = {	#Option B: Go back
		name = magic_estate.goback
		ai_chance = {
			factor = 5
		}
		country_event = { id = magic_estate.1 days = 0 }
	}
	
}


#Scrying
#Scrying
country_event = {
	id = magic_estate.5
	title = magic_estate.5.t
	desc = magic_estate.5.d
	picture = CITY_VIEW_eventPicture
	
	is_triggered_only = yes
	
	immediate = {
		hidden_effect = { 
			count_provinces_owned_by_rivals_effect = {
				which = amount_of_rival_provinces
			}
		}
		hidden_effect = { 
			count_provinces_owned_by_neighbours_effect = {
				which = amount_of_neighbour_provinces
			}
		}
		hidden_effect = { 
			count_provinces_owned_by_country_effect = {
				which = amount_of_owned_provinces
			}
		}
		hidden_effect = { 
			count_provinces_owned_by_war_enemies_effect = {
				which = amount_of_war_enemy_provinces
			}
		}
	}
	
	option = {	#Spy on our rivals
		name = magic_estate.5.a
		ai_chance = {
			factor = 50
		}
		
		trigger = {
			any_rival_country  = {
				exists = yes
			}
		}
		
		add_country_modifier = {
			name = magic_estate_scrying_rivals
			duration = 365
		}
		every_rival_country = {
			remove_fow = 12
		}

		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_CASTED_SPELLS
			influence = -10
			duration = 3650
		}
		
		if = {
			limit = {
				check_variable = { which = amount_of_rival_provinces value = 1 }
				NOT = { check_variable = { which = amount_of_rival_provinces value = 5 } }
			}
			add_years_of_income = -0.1
			add_adm_power = -25
			add_dip_power = -25
		}
		if = {
			limit = {
				check_variable = { which = amount_of_rival_provinces value = 5 }
				NOT = { check_variable = { which = amount_of_rival_provinces value = 10 } }
			}
			add_years_of_income = -0.2
			add_adm_power = -30	
			add_dip_power = -30
		}
		if = {
			limit = {
				check_variable = { which = amount_of_rival_provinces value = 10 }
				NOT = { check_variable = { which = amount_of_rival_provinces value = 15 } }
			}
			add_years_of_income = -0.3
			add_adm_power = -35
			add_dip_power = -35
		}
		if = {
			limit = {
				check_variable = { which = amount_of_rival_provinces value = 15 }
				NOT = { check_variable = { which = amount_of_rival_provinces value = 20 } }
			}
			add_years_of_income = -0.4
			add_adm_power = -40
			add_dip_power = -40
		}
		if = {
			limit = {
				check_variable = { which = amount_of_rival_provinces value = 20 }
				NOT = { check_variable = { which = amount_of_rival_provinces value = 30 } }
			}
			add_years_of_income = -0.5
			add_adm_power = -45	
			add_dip_power = -45
		}
		if = {
			limit = {
				check_variable = { which = amount_of_rival_provinces value = 30 }
				NOT = { check_variable = { which = amount_of_rival_provinces value = 40 } }
			}
			add_years_of_income = -0.6
			add_adm_power = -50
			add_dip_power = -50
		}
		if = {
			limit = {
				check_variable = { which = amount_of_rival_provinces value = 40 }
				NOT = { check_variable = { which = amount_of_rival_provinces value = 50 } }
			}
			add_years_of_income = -0.7
			add_adm_power = -55	
			add_dip_power = -55
		}
		if = {
			limit = {
				check_variable = { which = amount_of_rival_provinces value = 50 }
				NOT = { check_variable = { which = amount_of_rival_provinces value = 60 } }
			}
			add_years_of_income = -0.8
			add_adm_power = -60
			add_dip_power = -60
		}
		if = {
			limit = {
				check_variable = { which = amount_of_rival_provinces value = 60 }
				NOT = { check_variable = { which = amount_of_rival_provinces value = 70 } }
			}
			add_years_of_income = -0.9
			add_adm_power = -65
			add_dip_power = -65
		}
		if = {
			limit = {
				check_variable = { which = amount_of_rival_provinces value = 70 }
				NOT = { check_variable = { which = amount_of_rival_provinces value = 80 } }
			}
			add_years_of_income = -1
			add_adm_power = -70	
			add_dip_power = -70
		}
		if = {
			limit = {
				check_variable = { which = amount_of_rival_provinces value = 80 }
				NOT = { check_variable = { which = amount_of_rival_provinces value = 90 } }
			}
			add_years_of_income = -1.1
			add_adm_power = -75
			add_dip_power = -75
		}
		if = {
			limit = {
				check_variable = { which = amount_of_rival_provinces value = 90 }
				NOT = { check_variable = { which = amount_of_rival_provinces value = 100 } }
			}
			add_years_of_income = -1.2
			add_adm_power = -80
			add_dip_power = -80
		}
		if = {
			limit = {
				check_variable = { which = amount_of_rival_provinces value = 100 }
				NOT = { check_variable = { which = amount_of_rival_provinces value = 200 } }
			}
			add_years_of_income = -1.3
			add_adm_power = -85
			add_dip_power = -85
		}
		if = {
			limit = {
				check_variable = { which = amount_of_rival_provinces value = 200 }
				NOT = { check_variable = { which = amount_of_rival_provinces value = 300 } }
			}
			add_years_of_income = -1.4
			add_adm_power = -90
			add_dip_power = -90
		}
		if = {
			limit = {
				check_variable = { which = amount_of_rival_provinces value = 300 }
				NOT = { check_variable = { which = amount_of_rival_provinces value = 400 } }
			}
			add_years_of_income = -1.5
			add_adm_power = -95
			add_dip_power = -95
		}
		if = {
			limit = {
				check_variable = { which = amount_of_rival_provinces value = 400 }
				NOT = { check_variable = { which = amount_of_rival_provinces value = 500 } }
			}
			add_years_of_income = -1.6
			add_adm_power = -100
			add_dip_power = -100
		}
		if = {
			limit = {
				check_variable = { which = amount_of_rival_provinces value = 500 }
				NOT = { check_variable = { which = amount_of_rival_provinces value = 1000 } }
			}
			add_years_of_income = -3
			add_adm_power = -150
			add_dip_power = -150
		}
	}
	
	option = {	#Spy on our neighbours
		name = magic_estate.5.b
		ai_chance = {
			factor = 50
		}
		
		#Check to see if the country actually has any neighbours
		trigger = {
			any_country = {
				is_neighbor_of = ROOT
			}
		}
		

		add_country_modifier = {
			name = magic_estate_scrying_neighbours
			duration = 365
		}
		every_country = {
			limit = { 
				is_neighbor_of  = ROOT 
			}
			remove_fow = 12
		}
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_CASTED_SPELLS
			influence = -10
			duration = 3650
		}
		if = {
			limit = {
				check_variable = { which = amount_of_neighbour_provinces value = 1 }
				NOT = { check_variable = { which = amount_of_neighbour_provinces value = 5 } }
			}
			add_years_of_income = -0.1
			add_adm_power = -50	#adm power is the administrative cost of watching all these scrying balls
		}
		if = {
			limit = {
				check_variable = { which = amount_of_neighbour_provinces value = 5 }
				NOT = { check_variable = { which = amount_of_neighbour_provinces value = 10 } }
			}
			add_years_of_income = -0.2
			add_adm_power = -60	
		}
		if = {
			limit = {
				check_variable = { which = amount_of_neighbour_provinces value = 10 }
				NOT = { check_variable = { which = amount_of_neighbour_provinces value = 15 } }
			}
			add_years_of_income = -0.3
			add_adm_power = -70
		}
		if = {
			limit = {
				check_variable = { which = amount_of_neighbour_provinces value = 15 }
				NOT = { check_variable = { which = amount_of_neighbour_provinces value = 20 } }
			}
			add_years_of_income = -0.4
			add_adm_power = -80
		}
		if = {
			limit = {
				check_variable = { which = amount_of_neighbour_provinces value = 20 }
				NOT = { check_variable = { which = amount_of_neighbour_provinces value = 30 } }
			}
			add_years_of_income = -0.5
			add_adm_power = -90	
		}
		if = {
			limit = {
				check_variable = { which = amount_of_neighbour_provinces value = 30 }
				NOT = { check_variable = { which = amount_of_neighbour_provinces value = 40 } }
			}
			add_years_of_income = -0.6
			add_adm_power = -100	
		}
		if = {
			limit = {
				check_variable = { which = amount_of_neighbour_provinces value = 40 }
				NOT = { check_variable = { which = amount_of_neighbour_provinces value = 50 } }
			}
			add_years_of_income = -0.7
			add_adm_power = -110	
		}
		if = {
			limit = {
				check_variable = { which = amount_of_neighbour_provinces value = 50 }
				NOT = { check_variable = { which = amount_of_neighbour_provinces value = 60 } }
			}
			add_years_of_income = -0.8
			add_adm_power = -120
		}
		if = {
			limit = {
				check_variable = { which = amount_of_neighbour_provinces value = 60 }
				NOT = { check_variable = { which = amount_of_neighbour_provinces value = 70 } }
			}
			add_years_of_income = -0.9
			add_adm_power = -130	
		}
		if = {
			limit = {
				check_variable = { which = amount_of_neighbour_provinces value = 70 }
				NOT = { check_variable = { which = amount_of_neighbour_provinces value = 80 } }
			}
			add_years_of_income = -1
			add_adm_power = -140	
		}
		if = {
			limit = {
				check_variable = { which = amount_of_neighbour_provinces value = 80 }
				NOT = { check_variable = { which = amount_of_neighbour_provinces value = 90 } }
			}
			add_years_of_income = -1.1
			add_adm_power = -150
		}
		if = {
			limit = {
				check_variable = { which = amount_of_neighbour_provinces value = 90 }
				NOT = { check_variable = { which = amount_of_neighbour_provinces value = 100 } }
			}
			add_years_of_income = -1.2
			add_adm_power = -160
		}
		if = {
			limit = {
				check_variable = { which = amount_of_neighbour_provinces value = 100 }
				NOT = { check_variable = { which = amount_of_neighbour_provinces value = 200 } }
			}
			add_years_of_income = -1.3
			add_adm_power = -170
		}
		if = {
			limit = {
				check_variable = { which = amount_of_neighbour_provinces value = 200 }
				NOT = { check_variable = { which = amount_of_neighbour_provinces value = 300 } }
			}
			add_years_of_income = -1.4
			add_adm_power = -180
		}
		if = {
			limit = {
				check_variable = { which = amount_of_neighbour_provinces value = 300 }
				NOT = { check_variable = { which = amount_of_neighbour_provinces value = 400 } }
			}
			add_years_of_income = -1.5
			add_adm_power = -190
		}
		if = {
			limit = {
				check_variable = { which = amount_of_neighbour_provinces value = 400 }
				NOT = { check_variable = { which = amount_of_neighbour_provinces value = 500 } }
			}
			add_years_of_income = -1.6
			add_adm_power = -200
		}
		if = {
			limit = {
				check_variable = { which = amount_of_neighbour_provinces value = 500 }
				NOT = { check_variable = { which = amount_of_neighbour_provinces value = 1000 } }
			}
			add_years_of_income = -3
			add_adm_power = -300
		}
	}
	
	option = {	#Scry on internal dissidents
		name = magic_estate.5.c
		ai_chance = {
			factor = 50
		}
		add_country_modifier = {
			name = magic_estate_scrying_internal_dissidents
			duration = 365
		}
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_CASTED_SPELLS
			influence = -10
			duration = 3650
		}
		
		if = {
			limit = {
				check_variable = { which = amount_of_owned_provinces value = 1 }
				NOT = { check_variable = { which = amount_of_owned_provinces value = 5 } }
			}
			add_years_of_income = -0.1
			add_adm_power = -50	#adm power is the administrative cost of watching all these scrying balls
		}
		if = {
			limit = {
				check_variable = { which = amount_of_owned_provinces value = 5 }
				NOT = { check_variable = { which = amount_of_owned_provinces value = 10 } }
			}
			add_years_of_income = -0.2
			add_adm_power = -60	
		}
		if = {
			limit = {
				check_variable = { which = amount_of_owned_provinces value = 10 }
				NOT = { check_variable = { which = amount_of_owned_provinces value = 15 } }
			}
			add_years_of_income = -0.3
			add_adm_power = -70
		}
		if = {
			limit = {
				check_variable = { which = amount_of_owned_provinces value = 15 }
				NOT = { check_variable = { which = amount_of_owned_provinces value = 20 } }
			}
			add_years_of_income = -0.4
			add_adm_power = -80
		}
		if = {
			limit = {
				check_variable = { which = amount_of_owned_provinces value = 20 }
				NOT = { check_variable = { which = amount_of_owned_provinces value = 30 } }
			}
			add_years_of_income = -0.5
			add_adm_power = -90	
		}
		if = {
			limit = {
				check_variable = { which = amount_of_owned_provinces value = 30 }
				NOT = { check_variable = { which = amount_of_owned_provinces value = 40 } }
			}
			add_years_of_income = -0.6
			add_adm_power = -100	
		}
		if = {
			limit = {
				check_variable = { which = amount_of_owned_provinces value = 40 }
				NOT = { check_variable = { which = amount_of_owned_provinces value = 50 } }
			}
			add_years_of_income = -0.7
			add_adm_power = -110	
		}
		if = {
			limit = {
				check_variable = { which = amount_of_owned_provinces value = 50 }
				NOT = { check_variable = { which = amount_of_owned_provinces value = 60 } }
			}
			add_years_of_income = -0.8
			add_adm_power = -120
		}
		if = {
			limit = {
				check_variable = { which = amount_of_owned_provinces value = 60 }
				NOT = { check_variable = { which = amount_of_owned_provinces value = 70 } }
			}
			add_years_of_income = -0.9
			add_adm_power = -130	
		}
		if = {
			limit = {
				check_variable = { which = amount_of_owned_provinces value = 70 }
				NOT = { check_variable = { which = amount_of_owned_provinces value = 80 } }
			}
			add_years_of_income = -1
			add_adm_power = -140	
		}
		if = {
			limit = {
				check_variable = { which = amount_of_owned_provinces value = 80 }
				NOT = { check_variable = { which = amount_of_owned_provinces value = 90 } }
			}
			add_years_of_income = -1.1
			add_adm_power = -150
		}
		if = {
			limit = {
				check_variable = { which = amount_of_owned_provinces value = 90 }
				NOT = { check_variable = { which = amount_of_owned_provinces value = 100 } }
			}
			add_years_of_income = -1.2
			add_adm_power = -160
		}
		if = {
			limit = {
				check_variable = { which = amount_of_owned_provinces value = 100 }
				NOT = { check_variable = { which = amount_of_owned_provinces value = 200 } }
			}
			add_years_of_income = -1.3
			add_adm_power = -170
		}
		if = {
			limit = {
				check_variable = { which = amount_of_owned_provinces value = 200 }
				NOT = { check_variable = { which = amount_of_owned_provinces value = 300 } }
			}
			add_years_of_income = -1.4
			add_adm_power = -180
		}
		if = {
			limit = {
				check_variable = { which = amount_of_owned_provinces value = 200 }
				NOT = { check_variable = { which = amount_of_owned_provinces value = 300 } }
			}
			add_years_of_income = -1.5
			add_adm_power = -190
		}
		if = {
			limit = {
				check_variable = { which = amount_of_owned_provinces value = 300 }
				NOT = { check_variable = { which = amount_of_owned_provinces value = 400 } }
			}
			add_years_of_income = -1.6
			add_adm_power = -200
		}
		if = {
			limit = {
				check_variable = { which = amount_of_owned_provinces value = 400 }
				NOT = { check_variable = { which = amount_of_owned_provinces value = 500 } }
			}
			add_years_of_income = -1.7
			add_adm_power = -210
		}
		if = {
			limit = {
				check_variable = { which = amount_of_owned_provinces value = 500 }
				NOT = { check_variable = { which = amount_of_owned_provinces value = 1000 } }
			}
			add_years_of_income = -3
			add_adm_power = -300
		}
	}
	
	option = {	#Scry to aid the war effort
		name = magic_estate.5.dd
		ai_chance = {
			factor = 50
		}
		
		trigger = {
			is_at_war = yes
		}
		
		add_country_modifier = {
			name = magic_estate_scrying_military_intelligence
			duration = 365
		}
		every_country = {
			limit = { 
				war_with = ROOT 
			}
			remove_fow = 12
		}
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_CASTED_SPELLS
			influence = -10
			duration = 3650
		}
	
		if = {
			limit = {
				check_variable = { which = amount_of_war_enemy_provinces value = 1 }
				NOT = { check_variable = { which = amount_of_war_enemy_provinces value = 5 } }
			}
			add_years_of_income = -0.1
			add_adm_power = -25	#adm power is the administrative cost of watching all these scrying balls
			add_mil_power = -25
		}
		if = {
			limit = {
				check_variable = { which = amount_of_war_enemy_provinces value = 5 }
				NOT = { check_variable = { which = amount_of_war_enemy_provinces value = 10 } }
			}
			add_years_of_income = -0.2
			add_adm_power = -30	
			add_mil_power = -30
		}
		if = {
			limit = {
				check_variable = { which = amount_of_war_enemy_provinces value = 10 }
				NOT = { check_variable = { which = amount_of_war_enemy_provinces value = 15 } }
			}
			add_years_of_income = -0.3
			add_adm_power = -35
			add_mil_power = -35
		}
		if = {
			limit = {
				check_variable = { which = amount_of_war_enemy_provinces value = 15 }
				NOT = { check_variable = { which = amount_of_war_enemy_provinces value = 20 } }
			}
			add_years_of_income = -0.4
			add_adm_power = -40
			add_mil_power = -40
		}
		if = {
			limit = {
				check_variable = { which = amount_of_war_enemy_provinces value = 20 }
				NOT = { check_variable = { which = amount_of_war_enemy_provinces value = 30 } }
			}
			add_years_of_income = -0.5
			add_adm_power = -45	
			add_mil_power = -45
		}
		if = {
			limit = {
				check_variable = { which = amount_of_war_enemy_provinces value = 30 }
				NOT = { check_variable = { which = amount_of_war_enemy_provinces value = 40 } }
			}
			add_years_of_income = -0.6
			add_adm_power = -50
			add_mil_power = -50
		}
		if = {
			limit = {
				check_variable = { which = amount_of_war_enemy_provinces value = 40 }
				NOT = { check_variable = { which = amount_of_war_enemy_provinces value = 50 } }
			}
			add_years_of_income = -0.7
			add_adm_power = -55	
			add_mil_power = -55
		}
		if = {
			limit = {
				check_variable = { which = amount_of_war_enemy_provinces value = 50 }
				NOT = { check_variable = { which = amount_of_war_enemy_provinces value = 60 } }
			}
			add_years_of_income = -0.8
			add_adm_power = -60
			add_mil_power = -60
		}
		if = {
			limit = {
				check_variable = { which = amount_of_war_enemy_provinces value = 60 }
				NOT = { check_variable = { which = amount_of_war_enemy_provinces value = 70 } }
			}
			add_years_of_income = -0.9
			add_adm_power = -65
			add_mil_power = -65
		}
		if = {
			limit = {
				check_variable = { which = amount_of_war_enemy_provinces value = 70 }
				NOT = { check_variable = { which = amount_of_war_enemy_provinces value = 80 } }
			}
			add_years_of_income = -1
			add_adm_power = -70	
			add_mil_power = -70
		}
		if = {
			limit = {
				check_variable = { which = amount_of_war_enemy_provinces value = 80 }
				NOT = { check_variable = { which = amount_of_war_enemy_provinces value = 90 } }
			}
			add_years_of_income = -1.1
			add_adm_power = -75
			add_mil_power = -75
		}
		if = {
			limit = {
				check_variable = { which = amount_of_war_enemy_provinces value = 90 }
				NOT = { check_variable = { which = amount_of_war_enemy_provinces value = 100 } }
			}
			add_years_of_income = -1.2
			add_adm_power = -80
			add_mil_power = -80
		}
		if = {
			limit = {
				check_variable = { which = amount_of_war_enemy_provinces value = 100 }
				NOT = { check_variable = { which = amount_of_war_enemy_provinces value = 200 } }
			}
			add_years_of_income = -1.3
			add_adm_power = -85
			add_mil_power = -85
		}
		if = {
			limit = {
				check_variable = { which = amount_of_war_enemy_provinces value = 200 }
				NOT = { check_variable = { which = amount_of_war_enemy_provinces value = 300 } }
			}
			add_years_of_income = -1.4
			add_adm_power = -90
			add_mil_power = -90
		}
		if = {
			limit = {
				check_variable = { which = amount_of_war_enemy_provinces value = 200 }
				NOT = { check_variable = { which = amount_of_war_enemy_provinces value = 300 } }
			}
			add_years_of_income = -1.5
			add_adm_power = -95
			add_mil_power = -95
		}
		if = {
			limit = {
				check_variable = { which = amount_of_war_enemy_provinces value = 300 }
				NOT = { check_variable = { which = amount_of_war_enemy_provinces value = 400 } }
			}
			add_years_of_income = -1.6
			add_adm_power = -100
			add_mil_power = -100
		}
		if = {
			limit = {
				check_variable = { which = amount_of_war_enemy_provinces value = 400 }
				NOT = { check_variable = { which = amount_of_war_enemy_provinces value = 500 } }
			}
			add_years_of_income = -1.7
			add_adm_power = -105
			add_mil_power = -105
		}
		if = {
			limit = {
				check_variable = { which = amount_of_war_enemy_provinces value = 500 }
				NOT = { check_variable = { which = amount_of_war_enemy_provinces value = 1000 } }
			}
			add_years_of_income = -3
			add_adm_power = -150
			add_mil_power = -150
		}
	}
	
	option = {	#Option B: Go back
		name = magic_estate.goback
		ai_chance = {
			factor = 5
		}
		country_event = { id = magic_estate.1 days = 0 }
	}
}

#Aid Constructions
country_event = {
	id = magic_estate.6
	title = magic_estate.6.t
	desc = magic_estate.6.d
	picture = HOUSE_OF_TRADE_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = magic_estate.6.a
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0.25
				years_of_income = 0.5
			}
		}
		
		#Cost
		add_years_of_income = -0.5
		
		#Effect
		add_country_modifier = {
			name = magic_estate_aiding_construction
			duration = 365
		}
		
		
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_CASTED_SPELLS
			influence = -10
			duration = 3650
		}
	}
	
	option = {	#Option B: Go back
		name = magic_estate.goback
		ai_chance = {
			factor = 5
		}
		country_event = { id = magic_estate.1 days = 0 }
	}
	
}

#Empower Wards
country_event = {
	id = magic_estate.7
	title = magic_estate.7.t
	desc = magic_estate.7.d
	picture = CIVIL_WAR_eventPicture
	
	is_triggered_only = yes
	
	immediate = {
		hidden_effect = {
			 random_owned_province = {
				limit = {
					has_no_ward_province_modifiers = yes
					is_city = yes
				 }
				 save_event_target_as = ward_province_1
			 }
			 random_owned_province = {	#random province
				 limit = {
					 has_no_ward_province_modifiers = yes
					 is_city = yes
					 is_in_capital_area = yes
				 }
				 save_event_target_as = ward_province_1
			 }
			 random_owned_province = {
				 limit = {
					 has_no_ward_province_modifiers = yes
					 is_city = yes
				 }
				 save_event_target_as = ward_province_2
			 }
			 random_owned_province = {	#province with fort
				 limit = {
					 has_no_ward_province_modifiers = yes
					 is_city = yes
					 fort_level = 3
				 }
				 save_event_target_as = ward_province_2
			 }
			 random_owned_province = {
				 limit = {
					 has_no_ward_province_modifiers = yes
					 is_city = yes
				 }
				 save_event_target_as = ward_province_3
			 }
			 random_owned_province = {	#high dev
				 limit = {
					 has_no_ward_province_modifiers = yes
					 is_city = yes
					 development = 12
				 }
				 save_event_target_as = ward_province_3
			 }
		}
	}
	
	option = {	# Create Wards on
		name = magic_realm_abjuration.1.a
		ai_chance = {
			factor = 50
		}
		
		#Costs
		add_adm_power = -30
		add_years_of_income = -0.2
		
		#Effect
		event_target:ward_province_1 = {
			add_province_modifier = {
				name = magic_estate_ward 
				duration = 3650
			}
		}
		
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_CASTED_SPELLS
			influence = -10
			duration = 3650
		}
	}
	
	option = {	# Create Wards on
		name = magic_realm_abjuration.1.b
		ai_chance = {
			factor = 50
		}
		
		#Costs
		add_adm_power = -30
		add_years_of_income = -0.2
		
		#Effect
		event_target:ward_province_2 = {
			add_province_modifier = {
				name = magic_estate_ward 
				duration = 3650
			}
		}
		
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_CASTED_SPELLS
			influence = -10
			duration = 3650
		}
	}
	
	option = {	# Create Wards on
		name = magic_realm_abjuration.1.c
		ai_chance = {
			factor = 50
		}
		
		#Costs
		add_adm_power = -30
		add_years_of_income = -0.2
		
		#Effect
		event_target:ward_province_3 = {
			add_province_modifier = {
				name = magic_estate_ward 
				duration = 3650
			}
		}
		
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_CASTED_SPELLS
			influence = -10
			duration = 3650
		}
	}
	
	option = {	#Option B: Go back
		name = magic_estate.goback
		ai_chance = {
			factor = 5
		}
		country_event = { id = magic_estate.1 days = 0 }
	}
}

#Rite of Conception
country_event = {
	id = magic_estate.8
	title = magic_estate.8.t
	desc = magic_estate.8.d
	picture = NEW_HEIR_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = magic_estate.8.a
		trigger = {
			has_consort = yes
			has_heir = no
			has_government_attribute = heir
		}
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0.25
				years_of_income = 3
			}
		}
		
		#Cost
		add_years_of_income = -2
		
		#Effect
		if = {
			limit = {
				AND = {
					ruler_has_personality = mage_personality
					consort_has_personality = mage_personality
				}
			}
			random_list = {
				50 = {
					define_heir = {
						dynasty = ROOT
						claim = 100					
						hidden = yes
					}
					hidden_effect = { add_heir_personality = mage_personality }
				}
				40 = {
					
				}
				5 = {
					kill_ruler = yes
				}
				5 = {
					remove_consort = yes
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					ruler_has_personality = mage_personality
					consort_has_personality = mage_personality
				}
			}
			random_list = {
				30 = {
					define_heir = {
						dynasty = ROOT
						claim = 100		
						hidden = yes
					}
					hidden_effect = { add_heir_personality = mage_personality }
				}
				55 = {
					
				}
				5 = {
					kill_ruler = yes
				}
				10 = {
					remove_consort = yes
				}
			}
		}
		else = {
			random_list = {
				5 = {
					define_heir = {
						dynasty = ROOT
						claim = 100
						hidden = yes
					}
					hidden_effect = { add_heir_personality = mage_personality }
				}
				75 = {
					
				}
				5 = {
					kill_ruler = yes
				}
				15 = {
					remove_consort = yes
				}
			}
		}
		
		add_estate_influence_modifier = {
			estate = estate_mages
			desc = EST_VAL_CASTED_SPELLS
			influence = -10
			duration = 3650
		}
		
	}
	
	option = {	#Option B: Go back
		name = magic_estate.goback
		ai_chance = {
			factor = 5
		}
		country_event = { id = magic_estate.1 days = 0 }
	}
	
}