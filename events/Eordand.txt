namespace = eordand

country_event = { #ChangeGovernment
	id = eordand.1
	title = eordand.1.t
	desc = eordand.1.d
	picture = COURT_eventPicture
	
	is_triggered_only = yes
	
	option = {		# Despotic Monarchy
		name = "eordand.1.a"
		ai_chance = { 
			factor = 45
			modifier = {
				factor = 10
				tag = H13 #Sons of Dameria
			}
			modifier = {
				factor = 2
				OR = {
					government = monarchy
				}
			}
			modifier = {
				factor = 0.01
				government = H07
			}
		}	
		if = {
			limit = { NOT = { government = monarchy } }
			change_government = monarchy
		}
		add_government_reform = autocracy_reform
		
	}
	option = {		# Oligarchic Republic
		name = "eordand.1.b"
		ai_chance = { 
			factor = 15
			modifier = {
				factor = 2	#Having a poor ruler makes it likely they want an elective country
				OR = {
					NOT = { adm = 3 }
					NOT = { dip = 3 }
					NOT = { mil = 3 }
				}
			}
		}
		if = {
			limit = { NOT = { government = republic } }
			change_government = republic
		}
		add_government_reform = oligarchy_reform
	}
	option = {		# Merchant Republic
		name = "eordand.1.c"
		ai_chance = { 
			factor = 10 
			modifier = {
				factor = 300
				OR = {
					tag = H07 #House of Riches
				}
			}
		}
		if = {
			limit = { NOT = { government = republic } }
			change_government = republic
		}
		add_government_reform = merchants_reform
	}
	
	option = {		# Magocracy
		name = "eordand.1.dd"
		ai_chance = { 
			factor = 50 
			modifier = {
				factor = 1000
				OR = {
					government = theocracy
				}
			}
		}
		if = {
			limit = { NOT = { government = republic } }
			change_government = theocracy
		}
		add_government_reform = magocracy_reform
	}
}

country_event = { #ChangeCapital
	id = eordand.2
	title = eordand.2.t
	desc = eordand.2.d
	picture = GOLDEN_CITY_eventPicture
	
	is_triggered_only = yes
	
	option = { #Spring
		name = "eordand.2.a"
		trigger = {
				owns_core_province = 2007
		}
		ai_chance = {
			factor = 20
			modifier = {
				factor = 300
				OR = {
					tag = H01
					primary_culture = selpheregi
				}
			}
		}
		2007 = { #Arakeprun
			move_capital_effect = yes
		}
		add_country_modifier = {
			name = "eordand_spring_court"
			duration = -1
		}
	}
	option = { #Summer
			name = "eordand.2.b"
			trigger = {
				owns_core_province = 2037
			}
			ai_chance = {
			factor = 20
			modifier = {
				factor = 300
				OR = {
					tag = H02
					primary_culture = caamas
					}
				}
			}
			2037 = { #Murdkather
				move_capital_effect = yes
			}
			add_country_modifier = {
				name = "eordand_summer_court"
				duration = -1
			}
	}
	option = { #Autumn Court
		name = "eordand.2.c"
		trigger = {
				owns_core_province = 2159
		}
		ai_chance = {
			factor = 20
			modifier = {
				factor = 300
				OR = {
					tag = H10
					primary_culture = tuathak
				}
			}
		}
		2159 = { #Einnsag
			move_capital_effect = yes
		}
		add_country_modifier = {
			name = "eordand_autumn_court"
			duration = -1
		}
	}
	option = { #Winter Court
		name = "eordand.2.dd"
		trigger = {
				owns_core_province = 1775
		}
		ai_chance = {
			factor = 20
			modifier = {
				factor = 300
				OR = {
					tag = H16
					primary_culture = snecboth
				}
			}
		}
			1775 = { #Gemradcurt
				move_capital_effect = yes
			}
			add_country_modifier = {
				name = "eordand_winter_court"
				duration = -1
			}
	}
	option = { #Peitar
		name = "eordand.2.e"
		trigger = {
				owns_core_province = 2049
		}
		ai_chance = {
			factor = 20
			modifier = {
				factor = 300
				OR = {
					tag = H08
					primary_culture = peitar
				}
			}
		}
			2049 = { #Pelodard
				move_capital_effect = yes
			}
			add_country_modifier = {
				name = "eordand_domandrod"
				duration = -1
			}
	}
	option = { #NoChange
		name = "eordand.2.f"
		ai_chance = {
			factor = 0.1
		}
			add_adm_power = 50
			add_dip_power = 50
			add_mil_power = 50
	} 
}	