namespace = gnomes

country_event = {
	id = gnomes.1
	title = gnomes.1.t
	desc = gnomes.1.d
	picture = CITY_DEVELOPMENT_eventPicture
	
	is_triggered_only = yes	

	option = {
		name = gnomes.1.a
		add_country_modifier = {
			name = "nimscodd_resettling_the_homeland"
			duration = 18250 #50 years
		}
	}
}